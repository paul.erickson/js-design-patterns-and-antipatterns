import assert from 'assert';

import Test from '../Test.mjs';

export default class DemoTest extends Test {

  constructor() {
    super();
  }

  async "strings can be converted to numbers"() {
    Given: 'a string'
    const s = '9';
    When: 'I convert it to a number'
    const n = +s;
    Then: 'It stringifies to the same string'
    assert.equal(s.toString(), n.toString());
  }

}

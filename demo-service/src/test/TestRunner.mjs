import glob from 'glob';

import Test from './Test.mjs';

export default class TestRunner {

  static get instance() {
    if (this._instance === undefined) {
      this._instance = new this();
    }
    return this._instance;
  }

  constructor() {
    if (this.constructor._instance !== undefined) {
      throw `Can't instantiate more than one ${this.constructor.name} because it is a singleton.  Use ${this.constructor.name}.instance`;
    }
    this.pass = 0;
    this.fail = 0;
    console.clear();
    process.on('exit', () => {
      if (this.fail) {
        process.exitCode = 1;
      }
    });
  }

  /**
   * Scan the test directory, import everything, and run the Test classes
   */
  scan() {
    glob("src/test/**/*.mjs", {}, (error, filenames) => {
      if (error) {
        console.error(error);
      }
      Promise
        .all(filenames.map(filename => import(`../../${filename}`)
          .then(module => module.default)
          .then(Class => {
            if (typeof Class === 'function' && Class.prototype instanceof Test) {
              return this.run(Class);
            }
          })
        ))
        .then(modules => {
          console.log('————————————————————————————————————————————————————————————————————————————————');
          console.log(`${this.pass}/${this.pass + this.fail} tests passed`);
          console.info(`Scanned ${modules.length} modules and found ${this.tests.length} test classes`);
          console.log();
          process.exit(this.fail? 1 : 0);
        })
        .catch(console.log)
      ;
    });
  }

  /**
   * Add a test class and run all of its methods
   *
   * @param TestClass
   */
  async run(TestClass) {
    if (this.tests === undefined) {
      this.tests = [];
    }
    console.info(`\x1b[1m\x1b[4m${TestClass.name}\x1b[0m`);
    const test = new TestClass();
    this.tests.push(test);
    return Promise.all(Object
      .getOwnPropertyNames(Object.getPrototypeOf(test))
      .filter(name => name !== 'constructor')
      .map(name => test[name])
      .filter(property => typeof property === 'function')
      .map(async method => {
        try {
          await method.bind(test)();
          ++this.pass;
          console.info(`\x1b[32m✔ ${method.name}\x1b[0m`);
        } catch (e) {
          ++this.fail;
          console.info(`\x1b[31m✘ ${method.name}\x1b[0m`);
          console.error(e.stack || e)
        }
        console.groupEnd();
        return;
      }))
    ;
  }

}

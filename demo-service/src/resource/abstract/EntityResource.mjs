import HttpErrors from 'http-errors';

import Resource from './Resource.mjs';

export default class EntityResource extends Resource {

  constructor() {
    super()
  }

  /**
   * @private
   */
  async createEntity(json, ...ids) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @private
   */
  async retrieveEntity(...ids) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @private
   */
  async updateEntity(json, ...ids) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @private
   */
  async deleteEntity(...ids) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @override
   * @private
   */
  async handleDelete(req, res) {
    await this.deleteEntity(...this.getOrderedParams(req));
    res.sendStatus(204);
  }

  /**
   * @override
   * @private
   */
  async handleGet(req, res) {
    const entity = await this.retrieveEntity(...this.getOrderedParams(req));
    if (entity === undefined) {
      throw HttpErrors.NotFound();
    }
    if (entity.constructor.mediaSubtype) {
      res.type(entity.constructor.mimeType);
    }
    res.json(entity);
  }

  /**
   * @override
   * @private
   */
  async handleOptions(req, res) {
    res.header('Access-Control-Allow-Methods', 'DELETE, GET, OPTIONS, PATCH, PUT');
    res.sendStatus(204);
  }

  /**
   * @override
   * @private
   */
  async handlePatch(req, res) {
    const entity = await this.updateEntity(req.body, ...this.getOrderedParams(req));
    if (entity === undefined) {
      res.sendStatus(204);
    }
    else {
      res.json(entity);
    }
  }

  /**
   * @override
   * @private
   */
  async handlePut(req, res) {
    const entity = await this.createEntity(req.body, ...this.getOrderedParams(req));
    if (entity === undefined) {
      res.sendStatus(201);
    }
    else {
      res.status(201).json(entity);
    }
  }

}


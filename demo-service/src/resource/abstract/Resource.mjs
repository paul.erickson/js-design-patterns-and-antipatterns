import HttpErrors from 'http-errors';
import assert from 'assert';
import express from 'express';
import jwt from 'express-jwt';
import winston from 'winston';

import Singleton from '../../mixins/Singleton.mjs';
import config from '../../config.mjs';

/**
 * Base class for REST-style controllers with pretty sane defaults and middleware
 *
 * Set pathSegment property to map URLs (e.g. `this.pathSegment = '/widgets?'` matches requests to /widget or /widgets)
 * Set resources property to an array of subresources, if any (e.g. `this.resources = [new AcmeWidgetResource()]`)
 *
 * Override methods handleGet, handlePost, etc. for whichever verbs you want mapped.  _These methods must be async!_
 *
 * See also CollectionResource, EntityResource, and CrudResource for higher abstractions
 */
export default class Resource extends Singleton(Object) {

  constructor() {
    super();
    this.resources = [];
    this.router = express.Router({mergeParams: true})
      .use(express.json({strict: false, type: ['json', '+json']}))
      .use( (req, res, next) => this.logRequest(req, next) )
      .use( (req, res, next) => this.attachHeaders(res, next) )
      .use(jwt({
        algorithms: [ 'RS256', 'RS384', 'RS512', 'ES256', 'ES384', 'ES512' ],
        credentialsRequired: false,
        requestProperty: 'auth',
        secret: Buffer.from(config.jwtPublicKeyBase64, 'base64')
      }))
      .use( (req, res, next) => this.authorizeRequest(req, next) )
      .delete('/', (req, res, next) => this.handleDelete(req, res).catch(e => next(e)) )
      .get('/', (req, res, next) => this.handleGet(req, res).catch(e => next(e)) )
      .head('/', (req, res, next) => this.handleHead(req, res).catch(e => next(e)) )
      .options('/', (req, res, next) => this.handleOptions(req, res).catch(e => next(e)) )
      .patch('/', (req, res, next) => this.handlePatch(req, res).catch(e => next(e)) )
      .post('/', (req, res, next) => this.handlePost(req, res).catch(e => next(e)) )
      .put('/', (req, res, next) => this.handlePut(req, res).catch(e => next(e)) )
      .use( (err, req, res, next) => this.handleError(err, res) )
    ;
  }

  attach(router) {
    if (!this.pathSegment) {
      this.pathSegment = `/${this.constructor.name}`;
      winston.warn(`pathSegment not defined!  Defaulting to ${this.pathSegment}`);
    }
    router.use(this.pathSegment, this.router);
    if (this.resources) {
      this.resources.map(resource => resource.attach(this.router));
    }
    return this.router;
  }

  /**
   * @private
   */
  async handleDelete(req, res) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * Note that Express uses the GET handler to handle HEAD as well.  See https://github.com/expressjs/expressjs.com/issues/748
   *
   * @private
   */
  async handleGet(req, res) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * Note that Express uses the GET handler to handle HEAD as well.  See https://github.com/expressjs/expressjs.com/issues/748
   *
   * @private
   */
  async handleHead(req, res) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @private
   */
  async handleOptions(req, res) {
    res.sendStatus(204);
  }

  /**
   * @private
   */
  async handlePatch(req, res) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @private
   */
  async handlePost(req, res) {
    throw HttpErrors.MethodNotAllowed();
  }

  /**
   * @private
   */
  async handlePut(req, res) {
    throw HttpErrors.MethodNotAllowed();
  }

  logRequest(req, next) {
    winston.verbose(`Handling request ${req.method} ${req.originalUrl} from ${req.ip}`, {
      headers: req.headers,
      cookies: req.cookies,
      body: req.body
    });
    next();
  }

  attachHeaders(res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Expose-Headers', '*');
    if (Array.isArray(this.resources)) {
      res.header('Link', this.resources.map(resource => `<${resource.pathSegment}>; rel="item"; title="${resource.constructor.name}"`).join(','));
    }
    next();
  }

  authorizeRequest(req, next) {
    if (req.method === 'OPTIONS') {
      winston.debug('No authorization needed for OPTIONS request');
      return next();
    }
    if (config.securityDisabled === true) {
      winston.warn(`Security disabled!  Skipping authorization for ${this.constructor.name}`);
      return next();
    }
    if (req.authorized) {
      winston.debug(`${this.constructor.name}: Request was already authorized by a parent resource`);
      return next();
    }
    try {
      assert(req.auth, 'No authorization token provided');
      assert(!config.revokedSubjects.includes(req.auth.sub), 'Subject has been revoked');
      assert(req.auth.methods && req.auth.methods.includes(req.method), `${req.method} not in methods claim`);
      const audiences = Array.isArray(req.auth.aud) ? req.auth.aud : [req.auth.aud];
      assert(
        audiences
          .map(audience => new URL(audience))
          .some(url => config.origins.includes(url.origin) && req.originalUrl.startsWith(url.pathname))
        ,
        'No audience claim matching this server and resource'
      )
    } catch (error) {
      throw HttpErrors.Forbidden(error.message);
    }
    winston.verbose(`${this.constructor.name} authorized ${req.auth.sub} to ${req.method} ${req.path}`);
    req.authorized = true;
    next();
  }

  handleError(err, res) {
    winston.warn('Error handling request', err);
    winston.debug(err.stack);
    res.status(err.status || err.statusCode || 500).json(err);
  }

  // FIXME: params appear to be ordered, but this is probably unsafe.  cf. req.path
  getOrderedParams(req) {
    return Object.values(req.params);
  }

}

import winston from 'winston'

import CrudService from '../../service/abstract/CrudService.mjs';
import EntityResource from './EntityResource.mjs'

export default class CrudEntityResource extends EntityResource {

  /**
   * Request handling for basic CRUD operations (create, retrieve, update, delete)
   *
   * @param {class} EntityClass
   * @param {CrudService} [service]
   */
  constructor(EntityClass, service) {
    super();
    this.EntityClass = EntityClass;
    this.pathSegment = `/:${EntityClass.name}Id`;
    this.service = service || new CrudService(EntityClass);
  }

  /**
   *
   * @param object
   * @param id
   * @returns {Promise<void>}
   */
  async createEntity(object, id) {
    winston.info(`Submitting entity ${id}: ${JSON.stringify(object)}`);
    await this.service.create(new this.EntityClass(object), id);
  }

  /**
   *
   * @param id
   * @returns {Promise<EntityClass>}
   */
  async retrieveEntity(id) {
    winston.info(`Retrieving entity ${id}`);
    return this.service.retrieve(id);
  }

  /**
   *
   * @param object
   * @param id
   * @returns {Promise<void>}
   */
  async updateEntity(object, id) {
    winston.info(`Updating entity ${id}: ${JSON.stringify(object)}`);
    await this.service.update(object, id);
  }

  /**
   *
   * @param id
   * @returns {Promise<void>}
   */
  async deleteEntity(id) {
    winston.info(`Deleting entity ${id}`);
    await this.service.delete(id);
  }

}

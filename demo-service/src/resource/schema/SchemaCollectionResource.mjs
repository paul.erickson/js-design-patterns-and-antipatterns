import winston from 'winston';

import CollectionResource from '../abstract/CollectionResource.mjs';
import SchemaEntityResource from './SchemaEntityResource.mjs';
import Bag from '../../domain/Bag.mjs';
import BagCollection from '../../domain/BagCollection.mjs';
import Presenter from '../../domain/Presenter.mjs';
import PresenterCollection from '../../domain/PresenterCollection.mjs';

export default class SchemaCollectionResource extends CollectionResource {

  constructor() {
    super();
    this.pathSegment = '/schemas?';
    this.resources = [ new SchemaEntityResource() ];
    this.entities = [
      Bag,
      BagCollection,
      Presenter,
      PresenterCollection,
    ];
  }

  async retrieveAll() {
    return this.entities.map(entity => entity.schema);
  }

  authorizeRequest(req, next) {
    winston.debug(`Skipping authorization because ${this.constructor.name} is a public resource`);
    next();
  }

}

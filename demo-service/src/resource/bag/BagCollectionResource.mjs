import CrudCollectionResource from '../abstract/CrudCollectionResource.mjs'
import CrudService from '../../service/abstract/CrudService.mjs';
import BagCollection from '../../domain/BagCollection.mjs';
import Bag from '../../domain/Bag.mjs';
import BagEntityResource from './BagEntityResource.mjs';

export default class BagCollectionResource extends CrudCollectionResource {

  constructor(service = new CrudService(Bag)) {
    super(BagCollection, service, new BagEntityResource(service));
    this.pathSegment = '/bags?'
  }

}

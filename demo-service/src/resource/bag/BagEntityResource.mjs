import CrudEntityResource from '../abstract/CrudEntityResource.mjs'
import Bag from '../../domain/Bag.mjs'

export default class BagEntityResource extends CrudEntityResource {

  constructor(service) {
    super(Bag, service);
    this.pathSegment = '/:id';
  }

}

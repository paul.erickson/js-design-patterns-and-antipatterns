import winston from 'winston';

import PresenterCollectionResource from './presenter/PresenterCollectionResource.mjs';
import BagCollectionResource from './bag/BagCollectionResource.mjs';
import Resource from './abstract/Resource.mjs';
import SchemaCollectionResource from './schema/SchemaCollectionResource.mjs';
import manifest from '../../package.json';

export default class RootResource extends Resource {

  constructor() {
    super();
    this.pathSegment = '/';
    this.resources = [
      PresenterCollectionResource.instance,
      BagCollectionResource.instance,
      SchemaCollectionResource.instance,
    ];
  }

  /**
   * Make a little HTML doc to help folks discover the resources implemented by the service
   *
   * @override
   * @private
   */
  async handleGet(req, res) {
    function* traverse(resource, parentPath = ''){
      if (!resource) {
        return;
      }
      const path = `${parentPath}${resource.pathSegment}`.replace('?', '').replace('//', '/');
      yield `<li><a href="${path}">${path}</a></li>`;
      if (resource.resources) {
        yield `<ul>`;
        for (let child of resource.resources) {
          yield* traverse(child, path);
        }
        yield `</ul>`;
      }
    }
    res.set('Content-Type', 'text/html');
    res.write(`<h1>Demo Service v${manifest.version}</h1>`);
    res.write('<h2>Resources</h2>');
    res.write('<ul>');
    for (let each of traverse(this)) {
      res.write(each);
    }
    res.write('</ul>');
    res.end();
  }

  authorizeRequest(req, next) {
    winston.debug(`Skipping authorization because ${this.constructor.name} is a public resource`);
    next();
  }

}

import CrudEntityResource from '../abstract/CrudEntityResource.mjs'
import Presenter from '../../domain/Presenter.mjs'

export default class PresenterEntityResource extends CrudEntityResource {

  constructor(service) {
    super(Presenter, service);
    this.pathSegment = '/:id';
  }

}

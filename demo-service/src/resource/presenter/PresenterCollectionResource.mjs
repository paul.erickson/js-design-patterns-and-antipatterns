import CrudCollectionResource from '../abstract/CrudCollectionResource.mjs'
import CrudService from '../../service/abstract/CrudService.mjs';
import PresenterCollection from '../../domain/PresenterCollection.mjs';
import Presenter from '../../domain/Presenter.mjs';
import PresenterEntityResource from './PresenterEntityResource.mjs';

export default class PresenterCollectionResource extends CrudCollectionResource {

  constructor(service = new CrudService(Presenter)) {
    super(PresenterCollection, service, new PresenterEntityResource(service));
    this.pathSegment = '/presenters?'
  }

}

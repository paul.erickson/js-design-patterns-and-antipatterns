import DataStore from 'nedb-promise'
import HttpErrors from 'http-errors'
import winston from 'winston'

import config from '../../config'
import Entity from "../../domain/abstract/Entity";

export default class CrudDao {

  /**
   * Generic CRUD persistence for use as a starting point
   *
   * @param {class} EntityClass the type of object to handle
   */
  constructor(EntityClass = Entity) {
    this.EntityClass = EntityClass;
    this.db = config.dbPath? new DataStore({ filename: `${config.dbPath}/${EntityClass.name}.db`, autoload: true }) : new DataStore();
  }

  /**
   * Create or replace an existing entity
   * Assumes id is already valid, that caller created one if needed
   *
   * @param {EntityClass} object
   * @param id
   * @returns {Promise<void>}
   */
  async create(object, id) {
    await this.db.update({_id: id}, {...object, _id: id}, {upsert: true});
  }

  /**
   *
   * @param id
   * @returns {Promise<EntityClass>}
   */
  async retrieve(id) {
    const result = await this.db.findOne({_id: id});
    if (result) {
      return new this.EntityClass(result);
    }
    return undefined;
  }

  /**
   * Update only the specified properties without overwriting the whole document
   *
   * @param {EntityClass} object
   * @param id
   * @returns {Promise<void>}
   */
  async update(object, id) {
    const rowsAffected = await this.db.update({_id: id}, {$set: {...object,  _id: id}});
    if (rowsAffected === 1) {
      return;
    }
    if (rowsAffected === 0) {
      throw new HttpErrors.NotFound();
    }
    winston.error(`${rowsAffected} records affected after updating ${id} with document`, object);
  }

  /**
   * Delete an entity
   *
   * @param id
   * @returns {Promise<void>}
   */
  async delete(id) {
    winston.warn(`Deleting entity ${id}!  This is okay for proof-of-concept, but should probably change to soft delete`);
    await this.db.remove({_id: id});
  }

  /**
   * Retrieve array of entities, optionally filtering
   *
   * @param query
   * @returns {Promise<Array<EntityClass>>}
   */
  async retrieveAll(query = {}) {
    winston.info(`Finding entity by query ${JSON.stringify(query)}`);
    try {
      const result = await this.db.find(query);
      return result.map(each => new this.EntityClass(each));
    }
    catch(e) {
      winston.error(e);
      return [];
    }
  }

}

import Collection from './abstract/Collection.mjs';
import Bag from './Bag.mjs';

export default class BagCollection extends Collection(Bag) {

  static get mediaSubtype() {
    return `${Bag.mediaSubtype}.collection`;
  }

  constructor(collection) {
    super(collection);
  }

}

import Collection from './abstract/Collection.mjs';
import Presenter from './Presenter.mjs';

export default class PresenterCollection extends Collection(Presenter) {

  static get mediaSubtype() {
    return `${Presenter.mediaSubtype}.collection`;
  }

  constructor(collection) {
    super(collection);
  }

}

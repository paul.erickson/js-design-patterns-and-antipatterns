import Entity from './abstract/Entity.mjs';
import Schema from './metadata/Schema.mjs';
import config from '../config.mjs';

export default class Presenter extends Entity {

  static get schema() {
    if (this._schema === undefined) {
      this._schema = new Schema({
        "$schema": "http://json-schema.org/schema#",
        "$id": `${config.baseUrl}/schema/${this.mediaSubtype}`,
        "base": config.baseUrl,
        "title": "Presenter",
        "type": "object",
        "properties": {
          "id": {
            "readonly": true,
            "title": "ID",
            "type": "string"
          },
          "name": {
            "title": "Name",
            "type": "string"
          }
        },
        "links": [
          { "title": "Self", "href": "presenter/{id}", "rel": "self", "templateRequired": ["id"] },
          { "title": "Collection", "href": "presenter", "rel": "collection" }
        ]
      });
    }
    return this._schema;
  }

  constructor(object) {
    super(object);
  }

}

import Schema from '../metadata/Schema.mjs';
import JsonSchemaSupport from '../mixins/JsonSchemaSupport.mjs';

export default class Entity extends JsonSchemaSupport(Object) {

  /**
   * JSON Schema definition to describe this domain object
   *
   * @returns {Object}
   */
  static get schema() {
    return new Schema({
      "title": "Entity",
      "description": "Something that has an identity (intended only as a base class)",
      "type": "object",
      "properties": {
        "id": { "readonly": true, "title": "ID", "type": "string" },
      },
      "links": [
        { "href": ".", "rel": "self" },
      ]
    });
  }

  constructor(object) {
    super();
    Object.assign(this, object);
    if (this.id !== undefined) {
      this.id = this.id.toString();
    }
  }

};

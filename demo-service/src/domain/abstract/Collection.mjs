import JsonSchemaSupport from '../mixins/JsonSchemaSupport.mjs';
import config from '../../config.mjs';
import Schema from '../metadata/Schema.mjs';

export default (EntityClass) => class extends JsonSchemaSupport(Array) {

  static get EntityClass() { return EntityClass; }

  static get schema() {
    return new Schema({
      "$schema": "http://json-schema.org/schema#",
      "$id": `${config.baseUrl}/schema/${this.mediaSubtype}`,
      "base": config.baseUrl,
      "title": `${this.EntityClass.schema.title} Collection`,
      "type": "array",
      "items": this.EntityClass.schema
    });
  }

  constructor(collection) {
    super();
    Object.assign(this, collection);
  }

}

import Entity from './abstract/Entity.mjs';
import Schema from './metadata/Schema.mjs';
import config from '../config.mjs';

export default class Bag extends Entity {

  static get schema() {
    if (this._schema === undefined) {
      this._schema = new Schema({
        "$schema": "http://json-schema.org/schema#",
        "$id": `${config.baseUrl}/schema/${this.mediaSubtype}`,
        "base": config.baseUrl,
        "title": "Demo",
        "type": "object",
        "properties": {
          "id": {
            "readonly": true,
            "title": "ID",
            "type": "string"
          },
          "name": {
            "title": "Name",
            "type": "string"
          },
          "color": {
            "default": "brown",
            "title": "Color",
            "type": "string"
          },
          "livestreamed": {
            "default": false,
            "description": "Whether anybody planned ahead and brought a camera",
            "title": "livestreamed",
            "type": "boolean"
          },
          "presenterId": {
            "title": "Presenter ID",
            "type": "string"
          },
        },
        "links": [
          { "title": "Self", "href": "bag/{id}", "rel": "self", "templateRequired": ["id"] },
          { "title": "Collection", "href": "bag", "rel": "collection" },
          { "title": "Presenter", "href": "presenter/{presenterId}", "rel": "related", "templateRequired": ["presenterId"] },
        ]
      });
    }
    return this._schema;
  }

  constructor(object) {
    super(object);
  }

}

import JwtParser from '../parsers/JwtParser.mjs';
import Singleton from '../mixins/Singleton.mjs';

/**
 * Keeps track of authorization tokens
 *
 * Assumes authorization scheme of tokens with 'aud' and 'methods' corresponding to request URL and method
 *
 * Note: authorization tokens should never be persisted client-side
 */
export default class AuthorizationManager extends Singleton(Object) {

  constructor() {
    super();
    this.tokens = [];
  }

  /**
   * Put a token in the store
   *
   * @param jwt {string} encoded jwt string
   */
  addToken(jwt) {
    this.tokens.push([jwt, JwtParser.getPayload(jwt)]);
  }

  /**
   * Tries to find a token with an audience ('aud') matching or including the url
   *
   * @param url
   * @param method
   * @returns {string | null} encoded jwt string or null if not found
   */
  getToken(url, method) {
    if (!url || !method) {
      return null;
    }
    const tuple = this.tokens.find(([jwt, payload]) => url.startsWith(payload.aud) && payload.methods && payload.methods.includes(method));
    return tuple? tuple[0] : null;
  }

}

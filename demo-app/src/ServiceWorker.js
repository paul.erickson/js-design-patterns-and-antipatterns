/**
 * Service workers do not yet support modules
 *   See: https://bugs.chromium.org/p/chromium/issues/detail?id=824647
 *
 * TODO: I wanted to explore SW as authorization interceptor, but it doesn't seem like a good fit:
 *  - they should probably be optional, since the ink is not yet dry on the standard, and FF Private disables it
 *  - they can't dispatch events back to the webapp, except by way of MessageChannel?
 *  - no DOM access, so not an obvious fit for something that would drive an error modal
 * Maybe it's still worth having for the caching, notifications, etc.
 */
class ServiceWorker {

  /**
   * self refers to the context in which the service worker runs.
   * I will try to limit its usage to this constructor so that other methods are more obvious.
   */
  constructor() {
    caches.open('v1').then(cache => this.cache = cache);
    self.addEventListener('install', event => event.waitUntil(self.skipWaiting()));
    self.addEventListener('activate', event => event.waitUntil(self.clients.claim()));
    self.addEventListener('fetch', event => this.handleFetch(event));
  }

  /**
   *
   * @param {FetchEvent} event
   * @returns {Promise<void>}
   */
  async handleFetch(event) {
    event.respondWith(this.retrieveFromCacheOrNetwork(event.request));
  }

  /**
   *
   * @param {Request} request
   * @returns {Promise<Response>}
   */
  async retrieveFromCacheOrNetwork(request) {
    const cached = await caches.match(request);
    if (cached) {
      return cached;
    }
    const response = await fetch(request);
    this.cache.put(request, response.clone());
    return response
  }

}

ServiceWorker.instance = new ServiceWorker();

import DismissAction from '../actions/DismissAction.mjs';
import GlobalModalDialogComponent from '../components/GlobalModalDialogComponent.mjs';
import Singleton from '../mixins/Singleton.mjs';

export default class ErrorInterceptor extends Singleton(Object) {

  constructor() {
    super();
    window.addEventListener('error', e => this.handleError(e));
    window.addEventListener('unhandledrejection', e => this.handleError(e));
  }

  async handleError(error) {
    console.log(error);
    await GlobalModalDialogComponent.instance.promptUser(
      error.message || error.reason || error,
     null,
     [new DismissAction()]
    );
  }

}

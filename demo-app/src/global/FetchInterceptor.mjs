import Action from '../actions/abstract/Action.mjs';
import AuthorizationManager from '../authorization/AuthorizationManager.mjs';
import DismissAction from '../actions/DismissAction.mjs';
import GlobalModalDialogComponent from '../components/GlobalModalDialogComponent.mjs';
import LoginAction from '../actions/LoginAction.mjs';
import RetryAction from '../actions/RetryAction.mjs';
import Singleton from '../mixins/Singleton.mjs';

export default class FetchInterceptor extends Singleton(Object) {

  constructor() {
    super();
    this._fetch = window.fetch.bind(window);
    window.fetch = this.fetch.bind(this);
  }

  fetch() {
    const action = new Action(
      'Fetch',
      'Make a network request',
      () => {
        const request = new Request(...arguments);
        const authToken = AuthorizationManager.instance.getToken(request.url, request.method);
        if (authToken) {
          request.headers.set('Authorization', `Bearer ${authToken}`);
        }
        return this._fetch(request)
          .catch(e =>  GlobalModalDialogComponent.instance.promptUser(
            e.reason || e,
            null,
            [ new RetryAction(action), new DismissAction() ]
          ))
          .then(response => {
            if (response) {
              return response;
            }
            throw new Error('Cancelled by user');
          })
          .then(response => {
            if (response.status === 404) { // 404 is not really an error
              return response;
            }
            if (response.status >= 400 && response.status < 500) {
              return GlobalModalDialogComponent.instance.promptUser(response.statusText, null, [
                new LoginAction(action),
                new RetryAction(action),
                new DismissAction()
              ]).then(result => result || response);
            }
            return response;
          })
        ;
      }
    );
    return action.execute();
  }

}

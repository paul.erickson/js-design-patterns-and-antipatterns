export default (Superclass) => class extends Superclass {

  static get template() {
    if (this._template === undefined) {
      const propertyElements = Object
        .entries(this.schema.properties)
        .map(([propertyName, property]) => `
          <slot name="${propertyName}">
            <input
              ${property.readonly ? 'readonly disabled' : ''}
              class="cell"
              form="form"
              name="bind-data-${propertyName}"
              placeholder="${property.title}"
              title="${property.title}"
              type="${ (() => {
                if (propertyName === 'id') {
                  return 'hidden';
                }
                switch (property.format) {
                  case 'date': return 'date';
                  case 'date-time': return 'datetime-local';
                  case 'time': return 'time';
                }
                switch (property.type) {
                  case 'boolean': return 'checkbox';
                  case 'number': return 'number';
                  default: return 'text';
                }
              })() }"
            >
          </slot>
        `)
        .join('');

      this._template = document.createElement('template');
      this._template.innerHTML = `
        <style>
          :host {
            /* inherit grid explicitly, because it won't penetrate the shadow root otherwise */
            display: contents;
          }
          
          .grid {
            /* inherit grid explicitly, because it won't penetrate the shadow root otherwise */
            display: inherit;
          }
          
          .cell {
            border: 1px solid lightgrey;
          }
          
          .menu-dropdown {
            display: none;
            position: absolute;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
          }
          
          .menu-icon {
            border: none;
          }
          
          .menu-dropdown:hover {
            display: inline;
          }
          
          .menu-icon:hover + .menu-dropdown { 
            display: inline;
            position: absolute;
          }
          
          .menu-icon:hover {
            background-color: lightgrey;
          }
          
          input {
            border: none;
          }
          
          .menu:hover > .menu-icon {
            background-color: lightgrey;
          }
          
        </style>
        <div class="menu">
          <button class="menu-icon">&#8942;</button>
          <slot name="menu-dropdown" class="menu-dropdown"></slot>
        </div>
        <form name="form" class="grid"></form>
        <slot name="related-item-view"></slot>
        ${propertyElements}
      `;
    }
    return this._template;
  }

}

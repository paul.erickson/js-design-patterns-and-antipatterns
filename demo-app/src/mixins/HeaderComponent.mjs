export default (Superclass) => class extends (Superclass) {

  static get template() {
    if (this._template === undefined) {
      const headers = Object
        .entries(this.schema.properties)
        .filter(([key, value]) => key !== 'id')
        .map(([key, value]) => {
            return `
            <div class="cell">${value.title}</div>
          `;
          }
        ).join('');

      this._template = document.createElement('template');
      this._template.innerHTML = `
        <style>
          :host {
            /* inherit grid explicitly, because it won't penetrate the shadow root otherwise */
            display: contents;
          }
          .cell {
            grid-row-start: 1;
          }
        </style>
        <div class="cell"></div>${headers}
      `
    }
    return this._template;
  }

}

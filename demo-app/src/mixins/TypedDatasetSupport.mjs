/**
 * Data attributes always get stringified for the sake of HTML, but on the Javascript side we may want to remember the
 * type of data we put in, so that we (hopefully) can get it out in the same format.
 *
 * This proxies the Javascript interface (not HTML) to capture types when set and then use them to parse values on get.
 * HTML attribute values are mostly unchanged vs the normal DomStringMap behavior, except that object types are
 * converted to JSON to prevent data loss.  This includes objects, arrays, and nulls.
 *
 * @param Superclass (expected to be an HTMLElement)
 * @returns {{new(): {}, prototype: {}}}
 */
export default (Superclass) => class extends Superclass {

  constructor() {
    super(arguments);
    const propertyTypes = {};
    const proxy = new Proxy(this.dataset, {
      get: (obj, prop) => {
        switch (propertyTypes[prop]) {
          case 'boolean': return obj[prop] === 'true';
          case 'number': return Number(obj[prop]);
          case 'object': return JSON.parse(obj[prop]);
          default: return obj[prop];
        }
      },
      set: (obj, prop, value) => {
        propertyTypes[prop] = typeof value;
        if (typeof value === 'object') {
          obj[prop] = JSON.stringify(value);
        }
        else {
          obj[prop] = value;
        }
        return true;
      }
    });
    Object.defineProperty(this, 'dataset', { get: () => proxy });
  }

}

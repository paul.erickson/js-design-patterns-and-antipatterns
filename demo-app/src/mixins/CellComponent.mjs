export default (Superclass) => class extends Superclass {

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
      this._template.innerHTML = `
        <style>
          :host {
            /* inherit grid explicitly, because it won't penetrate the shadow root otherwise */
            display: flex;
            border: 1px solid lightgrey;
          }
          
          .menu-dropdown {
            display: none;
            position: absolute;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
          }
          
          .menu-icon {
            border: none;
          }
          
          .menu-dropdown:hover {
            display: inline;
          }
          
          .menu-icon:hover + .menu-dropdown { 
            display: inline;
            position: absolute;
          }
          
          .menu-icon:hover {
            background-color: lightgrey;
          }
          
          .menu:hover > .menu-icon {
            background-color: lightgrey;
          }
        </style>
        <div class="menu">
          <button class="menu-icon">&#8942;</button>
          <slot name="menu-dropdown" class="menu-dropdown"></slot>
        </div>
        ${this.schema.title}
      `
    }
    return this._template;
  }
}

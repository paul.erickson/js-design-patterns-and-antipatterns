export default (Superclass) => class extends Superclass {

  /**
   * Implementation is simplistic and should be overridden for trickier cases like dom elements.
   *
   * @returns {this} a copy of the object.
   */
  clone() {
    return Object.assign({}, this);
  }

}

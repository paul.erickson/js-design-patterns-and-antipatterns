export default class LinkParser {

  /**
   * Parse the links from an HTTP response into objects
   * Normalizes urls, treating response's url as base URL (if one is needed)
   */
  static getLinks(response) {
    if (!response.headers.has('Link')) {
      return [];
    }
    return response
      .headers
      .get('Link')
      .split(',')
      .map(this.parseLinkString)
      .map(link => ({...link, url: this.buildUrl(link.url, response.url).toString()}))
    ;
  }

  /**
   * Makes an absolute URL out of a relative path and base URL string
   *
   * @param {string} relativePath
   * @param {string} originalUrl
   * @returns {URL}
   */
  static buildUrl(relativePath, originalUrl) {
    const baseUrl = new URL(originalUrl);
    baseUrl.search = new URLSearchParams(); // clear out search part, in case it existed
    if (!baseUrl.pathname.endsWith('/')) {
      baseUrl.pathname += '/';
    }
    return new URL(relativePath, baseUrl);
  }

  /**
   * Parse a single link string (a Link header may have several) into an object
   */
  static parseLinkString(string) {
    return string
      .split(';')
      .reduce(
        (link, segment) => {
          // the URL part is xml-element-like; the rest are key=value
          try {
            link.url = /^\s*<(.*)>\s*/.exec(segment)[1];
          }
          catch(ignored) {
            const [key, val] = segment.split('=').map(each => each.trim().replace(/"/g, ''));
            if (key) {
              link[key] = val;
            }
          }
          return link;
        },
        {}
      )
    ;
  }

  /**
   * Parse the value of a Content-Type header into an object, having type, subtype, and optional attributes
   */
  static parseContentTypeString(string) {
    return string
      .split(';')
      .reduce(
        (object, segment, index) => {
          // the URL part comes first; the rest are key=value
          if (index === 0) {
            const [match, type, subtype, suffix] = /^\s*([^/;]+)\/([^;+]+)\+?([^;]+)?\s*/.exec(segment);
            object.type = type;
            object.subtype = subtype;
            object.suffix = suffix;
          }
          else {
            const [key, val] = segment.split('=').map(each => each.trim().replace(/"/g, ''));
            if (key) {
              object[key] = val;
            }
          }
          return object;
        },
        {}
      )
    ;
  }

}

export default class JwtParser {

  /**
   * Parses an encoded JWT string, returning the payload object
   * Does not validate signature
   *
   * @param jwt {string}
   * @returns {object}
   */
  static getPayload(jwt) {
    return JSON.parse(atob(jwt.split('.')[1]));
  }

}

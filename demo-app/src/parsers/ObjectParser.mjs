export default class ObjectParser {

  static async shasum(string) {
    if (typeof string === 'object') {
      string = JSON.stringify(string)
    }
    return crypto.subtle
      .digest('SHA-1', new TextEncoder().encode(string))
      .then(buffer => Array
        .from(new Uint8Array(buffer))
        .map(each => each.toString(16))
        .map(each => each.padStart(2, '0'))
        .join('')
      )
    ;
  }

}

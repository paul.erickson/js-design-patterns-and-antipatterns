import './components/Application.mjs';

navigator.serviceWorker
  .register('./ServiceWorker.js', {updateViaCache: 'none'})
  .then(reg => console.log(`Service worker registered Scope is ${reg.scope}`))
  .catch(error => console.log('Failed to register service worker',  error))
;

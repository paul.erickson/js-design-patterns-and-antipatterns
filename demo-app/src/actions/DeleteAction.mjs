import Action from './abstract/Action.mjs';

export default class DeleteAction extends Action {

  /**
   *
   * @param {HTMLElement} element
   * @param {URL} url
   */
  constructor(element, url) {
    super('Delete', 'Delete');
    this.element = element;
    this.url = url;
  }

  async execute() {
    // TODO: prompt for confirmation
    await fetch(this.url, {method: 'DELETE'});
    this.element.parentNode.removeChild(this.element);
    delete this.element;
  }

}

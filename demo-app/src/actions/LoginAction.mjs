import Action from './abstract/Action.mjs';
import GlobalModalDialogComponent from '../components/GlobalModalDialogComponent.mjs';
import DismissAction from './DismissAction.mjs';
import AuthorizationManager from '../authorization/AuthorizationManager.mjs';

export default class LoginAction extends Action {

  /**
   * Prompts the user to provide credentials and, optionally, chain an action to execute afterward
   * @param {Action?} action
   */
  constructor(action) {
    super('Login', 'Provide credentials');
    this.action = action;
  }

  execute() {
    const input = document.createElement('input');
    const submitAction = new Action('Submit', 'Submit', () => {
      AuthorizationManager.instance.addToken(input.value);
      if (this.action) {
        return this.action.execute();
      }
    });
    return GlobalModalDialogComponent.instance
      .promptUser('Login', input, [
        submitAction,
        new DismissAction()
      ])
    ;
  }

}

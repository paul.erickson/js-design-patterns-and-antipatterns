import Action from './abstract/Action.mjs';
import GlobalCollectionViewComponent from '../components/GlobalCollectionViewComponent.mjs';
import GlobalEntityViewComponent from '../components/GlobalEntityViewComponent.mjs';
import ComponentFactory from "../components/mediaTypes/ComponentFactory.mjs";

export default class BrowseAction extends Action {

  /**
   * Get entities from a URL, render, and prompt to select key of one
   *
   * @param {URL} url
   */
  constructor(url) {
    super('Browse', 'Browse');
    this.url = url;
  }

  async execute() {
    const response = await fetch(this.url);
    const component = await ComponentFactory.getComponentForResponse(response);
    component.renderOn(this);
  }

  // double-dispatch to get it to the right pane
  // TODO: maybe these should be handled differently or put elsewhere?
  renderCollection(component) {
    GlobalCollectionViewComponent.instance.render(component);
  }

  renderEntity(component) {
    GlobalEntityViewComponent.instance.render(component);
  }

}

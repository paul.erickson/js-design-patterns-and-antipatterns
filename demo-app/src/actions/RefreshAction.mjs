import Action from './abstract/Action.mjs';

export default class RefreshAction extends Action {

  /**
   * Given an HTML element and URL—each a representation of the other—fetches from the URL and replaces the element
   * with a new one.
   *
   * @param {HTMLElement} element
   * @param {URL} url
   */
  constructor(element, url) {
    super('Refresh', 'Retrieve the latest data');
    this.element = element;
    this.url = url;
  }

  async execute() {
    const response = await fetch(this.url);
    const object = await response.json();
    object.url = this.url.href.replace(/\/?$/, '/'); // it needs a trailing slash to be a base. TODO: trying to get away from this magic base URL property
    const newComponent = new this.element.constructor(object);
    this.element.parentNode.replaceChild(newComponent, this.element);
    delete this;
  }

}

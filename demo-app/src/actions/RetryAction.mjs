import Action from './abstract/Action.mjs';

export default class RetryAction extends Action {

  /**
   * @param {Action} action
   */
  constructor(action) {
    super('Retry', 'Retry the action');
    this.action = action;
  }

  execute() {
    return this.action.execute(arguments);
  }

}

import Action from './abstract/Action.mjs';
import GlobalCollectionViewComponent from "../components/GlobalCollectionViewComponent.mjs";
import ComponentFactory from "../components/mediaTypes/ComponentFactory.mjs";

export default class CreateAction extends Action {

  constructor(element, url) {
    super('Create', 'Create');
    this.element = element;
    this.url = url;
  }

  async execute() {
    await this.createEntity();
    await this.refreshCollectionView()
  }

  async createEntity() {
    const id = Date.now() + Math.floor(Math.random() * 10001);
    await fetch(this.url + id, {
      method: 'PUT',
      body: JSON.stringify(this.element.dataset),
      headers: {'Content-Type': 'application/json'}
    });
  }

  async refreshCollectionView() {
    const response = await fetch(this.url);
    const component = await ComponentFactory.getComponentForResponse(response);
    GlobalCollectionViewComponent.instance.render(component);
  }


}

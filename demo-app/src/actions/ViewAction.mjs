import Action from './abstract/Action.mjs';
import GlobalCollectionViewComponent from '../components/GlobalCollectionViewComponent.mjs';
import GlobalEntityViewComponent from '../components/GlobalEntityViewComponent.mjs';

export default class ViewAction extends Action {

  /**
   *
   * @param {Clonable(HTMLElement)} element
   */
  constructor(element) {
    super('Details', 'View Details');
    this.element = element;
  }

  async execute() {
    this.element.clone().renderOn(this);
  }

  // double-dispatch to get it to the right pane
  // TODO: maybe these should be handled differently or put elsewhere?
  renderCollection(component) {
    GlobalCollectionViewComponent.instance.render(component);
  }

  renderEntity(component) {
    GlobalEntityViewComponent.instance.render(component);
  }

}

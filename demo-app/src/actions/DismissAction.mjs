import Action from './abstract/Action.mjs';

export default class DismissAction extends Action {

  /**
   * Do nothing, returning nothing
   *
   */
  constructor() {
    super('Dismiss', 'Ignore and take no action');
  }

  execute() {
    return null;
  }

}

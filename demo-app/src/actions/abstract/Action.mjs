/**
 * Representation of something the user can do—general or specific—for example: retry another action, submit monthly
 * charges to CBS, login with NetID account, etc.
 *
 */
export default class Action {

  /**
   *
   * @param {string} label
   * @param {string} description
   * @param {function<void,*>?} callback
   */
  constructor(label, description, callback) {
    this.label = label;
    this.description = description;
    this.callback = callback;
    this.id = `action-${Math.random()}`; // TODO: a uuid would be proper
  }

  execute() {
    return this.callback? this.callback(arguments) : null;
  }

  /**
   * Make the action into an HTML button.
   * Can be made without onclick handler, e.g. if you'd rather execute it from a dialog onclose event.
   *
   * @param {boolean} executeOnClick
   * @returns {HTMLElement}
   */
  toButton(executeOnClick = true) {
    const button = document.createElement('button');
    button.value = this.id;
    button.innerText = this.label;
    if (executeOnClick) {
      button.onclick = () => this.execute();
    }
    button.title = this.description || this.label;
    return button;
  }

}

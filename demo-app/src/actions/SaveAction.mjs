import Action from './abstract/Action.mjs';

export default class SaveAction extends Action {

  /**
   * Save changes to an existing entity (PATCH)
   *
   * @param {HTMLElement} element
   * @param {URL} url
   */
  constructor(element, url) {
    super('Save', 'Save');
    this.element = element;
    this.url = url;
  }

  async execute() {
    await fetch( this.url, {
      method: 'PATCH',
      body: JSON.stringify(this.element.dataset),
      headers: {'Content-Type': 'application/json'}
    });
  }

}

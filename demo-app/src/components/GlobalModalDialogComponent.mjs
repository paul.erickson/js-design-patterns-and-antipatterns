import dialogPolyfill from 'https://cdn.jsdelivr.net/gh/paulerickson/dialog-polyfill@master/dist/dialog-polyfill.esm.js';

import BaseComponent from './abstract/BaseComponent.mjs';
import Singleton from '../mixins/Singleton.mjs';

/**
 * Intercepts error conditions arising from uncaught exceptions, unhandled promise rejections, and HTTP response codes
 * by way of a monkeypatched fetch API.
 *
 * Facilitates finding appropriate actions and presenting them to the user with a dialog.
 */
export default class GlobalModalDialogComponent extends Singleton(BaseComponent) {

  static get elementName() { return 'uw-global-modal-dialog'; }

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
    }
    return this._template;
  }

  constructor() {
    super();
    this.dialog = this.shadowRoot.appendChild(document.createElement('dialog'));
  }

  /**
   * Ask the user what to do
   */
  async promptUser(heading, bodyElement, actions) {
    this.dialog.innerHTML = `<h2>${heading || ''}</h2>`;
    const form = this.dialog.appendChild(document.createElement('form'));
    form.method = 'dialog';
    if (bodyElement) {
      form.appendChild(bodyElement);
    }
    const menu = form.appendChild(document.createElement('menu'));
    actions
      .map(action => action.toButton(false))
      .map(actionButton => document.createElement('li').appendChild(actionButton))
      .forEach(menuItem => menu.appendChild(menuItem))
    ;
    dialogPolyfill.registerDialog(this.dialog);
    this.dialog.showModal(); // TODO: this is still an experimental API
    return new Promise(resolve => {
      this.dialog.onclose = () => {
        const selectedAction = actions.find(action => action.id === this.dialog.returnValue);
        if (selectedAction) {
          resolve(selectedAction.execute());
        }
        else {
          resolve();
        }
      }
    });
  }

}

customElements.define(GlobalModalDialogComponent.elementName, GlobalModalDialogComponent);

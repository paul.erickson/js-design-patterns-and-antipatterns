import BaseComponent from './abstract/BaseComponent.mjs';
import Singleton from '../mixins/Singleton.mjs';

export default class GlobalEntityViewComponent extends Singleton(BaseComponent) {

  static get elementName() { return 'uw-global-entity-view'; }

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
    }
    return this._template;
  }

  constructor() {
    super();
  }

  render(component) {
    while(this.shadowRoot.firstChild) {
      this.shadowRoot.firstChild.remove();
    }
    this.shadowRoot.appendChild(component);
  }

}

customElements.define(GlobalEntityViewComponent.elementName, GlobalEntityViewComponent);

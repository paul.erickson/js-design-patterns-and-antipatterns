import 'https://cdn.my.wisc.edu/@myuw-web-components/myuw-app-bar@latest/myuw-app-bar.min.mjs';
import 'https://unpkg.com/@myuw-web-components/myuw-app-styles@1.2.3/dist/myuw-app-styles.min.mjs';
import 'https://unpkg.com/@myuw-web-components/myuw-drawer@1.2.0/dist/myuw-drawer.min.mjs';
import 'https://unpkg.com/@myuw-web-components/myuw-profile@1.3.2/dist/myuw-profile.min.mjs';

import BaseComponent from './abstract/BaseComponent.mjs';
import ErrorInterceptor from '../global/ErrorInterceptor.mjs';
import FetchInterceptor from '../global/FetchInterceptor.mjs';
import GlobalCollectionViewComponent from './GlobalCollectionViewComponent.mjs';
import GlobalModalDialogComponent from './GlobalModalDialogComponent.mjs';
import RootResourceBrowserComponent from './RootResourceBrowserComponent.mjs';

export default class Application extends BaseComponent {

  static get elementName() { return 'uw-demo-app'; }

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
      this._template.innerHTML = `
        <style>
          * {
            font-family: quicksand, sans-serif;
          }
          header {
            position: fixed;
            top: 0px;
            left: 0px;
            width: 100vw;
          }
          main {
            padding-top: 64px;
          }
          footer {
          }
        </style>
        <header>
          <myuw-app-bar app-name="Demo App" app-url="/">
            <myuw-drawer slot="myuw-navigation">
              <myuw-drawer-link slot="myuw-drawer-links" name="Home" icon="home" href="#"></myuw-drawer-link>
            </myuw-drawer slot="myuw-navigation">
            <myuw-profile slot="myuw-profile" login-url="#" logout-url="#"></myuw-profile>
          </myuw-app-bar>
        </header>
        <main>
          <${RootResourceBrowserComponent.elementName}></${RootResourceBrowserComponent.elementName}>
          <section>
            <${GlobalCollectionViewComponent.elementName}></${GlobalCollectionViewComponent.elementName}>
          </section>
          <${GlobalModalDialogComponent.elementName}></${GlobalModalDialogComponent.elementName}>
        </main>
        <footer>
          <slot name="version"></slot>
        </footer>
      `;
    }
    return this._template;
  }

  constructor() {
    super();
    // Initialize profile component
    document.dispatchEvent(new CustomEvent('myuw-login', { detail: { person: { firstName: 'Anonymous' } } }));
    fetch('package.json')
      .then(response => response.json())
      .catch(() => ({version: '_dev'}))
      .then(project => {
        const element = document.createElement('a');
        element.href = project.homepage;
        element.innerText = `v${project.version}`;
        element.slot = 'version';
        this.appendChild(element);
      })
    ;
    this.errorInterceptor = ErrorInterceptor.instance;
    this.fetchInterceptor = FetchInterceptor.instance;
  }

}

customElements.define(Application.elementName, Application);

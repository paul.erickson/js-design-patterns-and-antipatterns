import BaseComponent from './BaseComponent.mjs';
import ComponentFactory from '../mediaTypes/ComponentFactory.mjs';
import LinkParser from '../../parsers/LinkParser.mjs';
import ViewAction from '../../actions/ViewAction.mjs';

/**
 * A web component for navigating API resources
 * Subclasses need to set a template
 * See also data-binding functionality in BaseComponent
 */
export default class ResourceBrowserBaseComponent extends BaseComponent {

  constructor(resource) {
    super(resource);
    this.shadowRoot.addEventListener('submit', event => {
      event.preventDefault();
      this.submit(event.target);
      return false;
    });
  }

  // TODO: query params!
  async submit(form) {
    const response = await fetch(form.url.value || this.dataset.url, {method: form.method.value || this.dataset.method});
    LinkParser
      .getLinks(response)
      .map(link => {
        const element = document.createElement('option');
        element.value = link.url;
        element.text = link.title;
        return element;
      })
      .forEach(element => form.url.list.appendChild(element))
    ;
    form.url.value = null; // clear the field so we see new options
    if (response.headers.get('Content-Type').includes('text/html')) {
      // Debugging: render unexpected HTML response
      // form.body.innerHTML = await response.text();
      return;
    }
    const component = await ComponentFactory.getComponentForResponse(response);
    const action = new ViewAction(component);
    action.execute();
  }

}


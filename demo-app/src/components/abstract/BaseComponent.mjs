import TypedDatasetSupport from '../../mixins/TypedDatasetSupport.mjs';

/**
 * Simple data-binding custom element
 *
 * Subclasses MUST define a class-level `template` attribute with a template node.  Example:
 *   MyComponent.template = document.currentScript.ownerDocument.getElementById('my-component-template');
 *
 * It provides basic two-way data binding between data attributes (i.e. attributes starting with 'data-'*) and dom
 * elements with a corresponding name (i.e. '[name="bind-data-X"]'.  If a 'change' event is dispatched from such an
 * element, the matching attribute will be updated.
 *
 * 1. Declare observed attributes (only 'data-[a-z]*' attributes get data binding)
 *   static get observedAttributes() {
 *     return ['data-spam', 'data-eggs', 'data-ham', 'somethingElse'];
 *   }
 *
 * 2. Bind elements of the dom by name (the bound attribute will go in value and textContent)
 *   <template id="my-template">
 *     <h1 name="bind-data-spam"></h1>
 *     <input name="bind-data-spam">
 *   </template>
 *
 * *Caveat: attribute names are treated as lowercase, and the dataset property name is expected to be lowercase, thus
 *   'data-highScore' and 'data-highscore' would both map to dataset.highscore, not to dataset.highScore
 *
 * TODO: the name attribute is probably not the appropriate attribute to use
 * TODO: refactor to mixins… DataBindingSupport, JsonSchemaSupport, etc. ?
 */
export default class BaseComponent extends TypedDatasetSupport(HTMLElement) {

  constructor(config) {
    super();
    this.attachShadow({mode: 'open'});
    this.shadowRoot.appendChild(this.constructor.template.content.cloneNode(true));
    for (const key in config) {
      this.dataset[key] = config[key];
      // FIXME: FF doesn't invoke the callback on data- attributes, so we call it explicitly here; duplicate in Chrome
      this.attributeChangedCallback(`data-${key}`, undefined, config[key]);
    }
    this.shadowRoot.addEventListener('change', event => {
      if (!event.target.name) {
        return;
      }
      const match = event.target.name.match(/^(?:bind-data-)([\w]*)$/);
      if (!match || !match[1]) {
        return;
      }
      switch (event.target.type) {
        case 'checkbox':
          this.dataset[match[1]] = event.target.checked;
          break;
        case 'date':
        case 'datetime-local':
        case 'time':
          this.dataset[match[1]] = event.target.valueAsNumber;
          break;
        default:
          this.dataset[match[1]] = event.target.value;
          break;
      }
    });
  }

  // subclasses should override this
  static get observedAttributes() {
    return [];
  }

  /**
   * Extends the standard API to include source, to support subscriptions to other elements
   * Attributes are always strings
   *
   * @param name
   * @param oldValue
   * @param newValue
   * @param source
   */
  attributeChangedCallback(name, oldValue, newValue, source) {
    if (newValue === oldValue) {
      return;
    }
    this
      .shadowRoot
      .querySelectorAll(`[name="bind-${name}"]`)
      .forEach(element => {
        switch (element.type) {
          case 'checkbox':
            element.checked = typeof newValue === 'boolean' ? newValue : newValue === 'true';
            break;
          case 'date':
          case 'datetime-local':
          case 'time':
            if (typeof newValue === 'number') {
              element.valueAsNumber = newValue;
            } else {
              element.value = newValue
            }
            break;
          default:
            element.textContent = newValue;
            element.value = newValue;
            break;
        }
      })
    ;
    if (this.subscribers && this.subscribers[name]) {
      this.subscribers[name].forEach(subscriber => subscriber.attributeChangedCallback(name, oldValue, newValue, this));
    }
  }

  observeAttribute(attributeName, subscriber) {
    if (this.subscribers === undefined) {
      this.subscribers = {};
    }
    if (this.subscribers[attributeName] === undefined) {
      this.subscribers[attributeName] = [];
    }
    this.subscribers[attributeName].push(subscriber);
    return this;
  }

};

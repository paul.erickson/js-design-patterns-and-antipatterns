import ResourceBrowserBaseComponent from "./abstract/ResourceBrowserBaseComponent.mjs";

/**
 * Probably just for testing/debugging
 */
export default class RootResourceBrowserComponent extends ResourceBrowserBaseComponent {

  static get elementName() { return 'uw-resource-browser'; }

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
      this._template.innerHTML = `
        <form>
          <fieldset>
            <legend>Root Resource</legend>
            <input list="urls" name="url" autocomplete="off" aria-label="URL">
            <datalist id="urls">
              <option value="http://localhost:3000">Demo Service (local)</option>
            </datalist>
            <input name="method" type="submit" value="GET">
          </fieldset>
          <output name="body"></output>
        </form>
      `;
    }
    return this._template;
  }

}

customElements.define(RootResourceBrowserComponent.elementName, RootResourceBrowserComponent);

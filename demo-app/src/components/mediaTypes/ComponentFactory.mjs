import LinkParser from '../../parsers/LinkParser.mjs';
import Schema from "./Schema.mjs";

export default class ComponentFactory {

  /**
   * Returns a web component made for the given media type (Content-Type header), falling back to a generic one
   * TODO: [x] Get JSON schema from profile attribute
   *       [ ] Fallback to list of schemas hardcoded with the app (e.g. for source data)
   *       [X] Fallback to generic data component
   *       [ ] In the _context_ of source data, pull schema from database?
   *
   * @param {Response} response
   * @returns {BaseComponent}
   */
  static async getComponentForResponse(response, mode) {
    const contentType = LinkParser.parseContentTypeString(response.headers.get('Content-Type'));
    const responseBody = await response.json();
    if (responseBody) {
      responseBody.url = responseBody.url || response.url; // TODO: is this just used as base for relative URLs? Should we read schema '$id' or 'self' link and use this as default value?
    }
    const schema = await Schema.fromContentType(contentType) || Schema.fromArray(responseBody) || Schema.fromObject(responseBody);
    if (schema.links === undefined) {
      /* Make Actions from HTTP links if schema doesn't specify links?  Make Actions from HTTP links only?  YAGNI?
      LinkParser
        .getLinks(response)
        .map(link => {
          // Action stuff
        })
      ;
      */
    }
    const ComponentClass = await schema.toCustomElementClass(mode);
    return new ComponentClass(responseBody);
  }

}

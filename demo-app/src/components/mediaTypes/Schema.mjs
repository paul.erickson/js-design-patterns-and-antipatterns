import ItemCollectionComponent from './ItemCollectionComponent.mjs';
import ItemComponent from './ItemComponent.mjs';
import ObjectParser from '../../parsers/ObjectParser.mjs';
import RowComponent from '../../mixins/RowComponent.mjs';
import HeaderComponent from '../../mixins/HeaderComponent.mjs';
import CellComponent from '../../mixins/CellComponent.mjs';

export default class Schema {

  /**
   * Retrieve the JSON Schema based on the profile of a contentType record (parsed as an object—see LinkParser)
   *
   * @param contentType
   * @returns {Promise<Schema>}
   */
  static async fromContentType(contentType) {
    if (contentType.profile === undefined) {
      return null;
    }
    return fetch(contentType.profile)
      .then(response => response.json())
      .then(json => new this(json))
      .catch(() => null)
    ;
  }

  /**
   * Generate a simple schema from a collection of objects based on the actual data
   * This assumes that the first record is an object and is representative of the whole collection
   *
   * @param array
   * @returns {Schema}
   */
  static fromArray(array) {
    if (!Array.isArray(array)) {
      return null;
    }
    return new this({
      "type": "array",
      "readonly": true,
      "items": this.fromObject(array[0])
    });
  }

  /**
   * Generate a simple schema from an object (e.g. because we can't find a real one)
   * The API is assumed to be read-only
   *
   * @param object
   * @returns {Schema}
   */
  static fromObject(object) {
    return new this({
      "type": "object",
      "readonly": true,
      "properties": Object.keys(object || {}).reduce(
        (properties, key) => {
          properties[key] = { "title": key, "type": "string", "readonly": true };
          return properties;
        },
        {}
      )
    });
  }

  constructor(object) {
    Object.assign(this, object);
  }

  /**
   * Creates and registers a new custom element with the DOM or returns the existing one, if already defined.
   * Makes some assumptions about uniqueness and naming.
   *
   * @returns {typeof BaseComponent}
   */
  async toCustomElementClass(mode) {
    const elementName = await this.generateElementName(mode);
    const ExistingComponent = window.customElements.get(elementName);
    if (ExistingComponent) {
      return ExistingComponent;
    }
    const BaseClass = this.type === 'array' ? ItemCollectionComponent : ItemComponent;
    const nestedSchema = await new this.constructor(this.items);
    const CollectionHeaderComponent = this.items ? await nestedSchema.toCustomElementClass('header'): undefined;
    const NestedComponent = this.items ? await nestedSchema.toCustomElementClass('row') : undefined;
    const schema = this;
    const NewComponent = (() => { 
      switch(mode) {
        case 'row': 
          return class extends RowComponent(BaseClass) {
            static get mode() { return 'row' }
            static get elementName() { return elementName; }
            static get schema() { return schema; }
          };
        case 'cell':
          return class extends CellComponent(BaseClass) {
            static get mode() { return 'cell' }
            static get elementComponent() { return NestedComponent; }
            static get elementName() { return elementName; }
            static get schema() { return schema; }
          };
        case 'header':
          return class extends HeaderComponent(BaseClass) {
            static get mode() { return 'header' }
            static get elementName() { return elementName; }
            static get schema() { return schema; }
          };
        default:
          return class extends BaseClass {
            static get mode() { return 'collection' }
            static get elementComponent() { return NestedComponent; }
            static get elementName() { return elementName; }
            static get headerComponent() { return CollectionHeaderComponent; }
            static get schema() { return schema; }
          };
      }
    })();
    window.customElements.define(elementName, NewComponent);
    await window.customElements.whenDefined(elementName);
    return NewComponent;
  }

  /**
   * If the schema has a title attribute—hopefully unique—munges it into a valid custom element name, otherwise makes up
   * a unique default by hashing the whole schema.
   *
   * @returns {string}
   */
  async generateElementName(mode) {
    return (this.title || await ObjectParser.shasum(this))
      .replace(/\W+/g, '-')
      .replace(/(^\W+|\W+$)/g, '')
      .toLowerCase()
      .replace(/^/, 'uw-')
      .concat(`-${mode}`)
    ;
  }

}

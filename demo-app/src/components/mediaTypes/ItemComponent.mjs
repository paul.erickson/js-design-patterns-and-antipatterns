import BaseComponent from '../abstract/BaseComponent.mjs';
import Clonable from '../../mixins/Clonable.mjs';
import DeleteAction from '../../actions/DeleteAction.mjs';
import RefreshAction from '../../actions/RefreshAction.mjs';
import SaveAction from '../../actions/SaveAction.mjs';
import ViewAction from '../../actions/ViewAction.mjs';
import Action from "../../actions/abstract/Action.mjs";
import CreateAction from "../../actions/CreateAction.mjs";
import ComponentFactory from "../mediaTypes/ComponentFactory.mjs";
import BrowseAction from "../../actions/BrowseAction.mjs";

export default class ItemComponent extends Clonable(BaseComponent) {
  
  static get elementName() { return 'uw-item'; }

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
    }
    return this._template;
  }
  
  /**
   * Information about the data type used for formatting, validation, etc.
   * Subclasses should override with their actual schema
   *
   * See also http://json-schema.org, https://brandur.org/elegant-apis
   */
  static get schema() {
    return {
      "type": "object",
      "properties": {
        "id": {"title": "ID", "type": "string"}
      }
    };
  }

  // See BaseComponent about attribute names and data mapping
  static get observedAttributes() {
    return Object.keys(this.schema.properties).map(key => `data-${key}`);
  }

  constructor(object) {
    super(object);
    this.populateDefaultValues();
    this.url = object.url;
    this.resolvedLinks = this.resolveLinks();
    this.shadowRoot.addEventListener('submit', event => {
      event.preventDefault();
      console.log('form was submitted for some reason; ignoring', event);
      return false;
    });
    this.appendPropertyInputs();
    this.replaceKeysWithRelatedEntities().then();
    if (this.constructor.mode === 'row' || this.constructor.mode === 'cell') {
      this.appendMenuDropdown();
    }
  }

  appendPropertyInputs() {
    Object.entries(this.constructor.schema.properties || [])
      .filter(([key, property]) => property.$ref)
      .forEach(([key, property]) => {
        if (object[key]) {
          const element = document.createElement('input');
          element.value = object[key];
          element.slot = key;
          this.appendChild(element);
        }
      })
    ;
  }

  appendMenuDropdown() {
    const menu = document.createElement('div');
    menu.slot = 'menu-dropdown';
    this.getActions()
      .map(action => action.toButton())
      .forEach(button => menu.appendChild(button))
    ;
    this.appendChild(menu);
  }

  populateDefaultValues() {
    Object
      .entries(this.constructor.schema.properties)
      .filter(([propertyName, property]) => property.default !== undefined)
      .filter(([propertyName, property]) => this.dataset[propertyName] === undefined)
      .forEach(([propertyName, property]) => this.dataset[propertyName] = property.default)
    ;
  }

  resolveLinks() {
    return (this.constructor.schema.links || [])
      .filter(link => {
        if (link.templateRequired === undefined || link.templateRequired.length === 0) {
          return true;
        }
        if (link.templateRequired.length === 1 && this.dataset[link.templateRequired[0]]) {
          return true;
        }
        // TODO: compound keys
      })
      .map(link => {
        if (link.templateRequired && link.templateRequired[0]) {
          return {...link, href: link.href.replace(`{${link.templateRequired[0]}}`, this.dataset[link.templateRequired[0]])};
        }
        return link;
      })
    ;
  }

  async replaceKeysWithRelatedEntities() {
    this.resolvedLinks
      .filter(link => link.rel === 'related')
      .forEach(link => {
        fetch(new URL(link.href, this.constructor.schema.base))
          .then(response => ComponentFactory.getComponentForResponse(response, 'cell'))
          .then(component => {
            component.slot = link.templateRequired[0];
            this.appendChild(component);
          })
        ;
      })
    ;
  }

  getActions() {
    /** @type {Action[]} */
    const actions = [];
    if (this.dataset['id'] !== undefined && this.constructor.mode === 'cell') {
      actions.push(new ViewAction(this));
    }
    const selfLink = this.resolvedLinks.find(link => link.rel === 'self' );
    // if (selfLink && selfLink.templateRequired) {
    //   const selfIdentifier = this.dataset[selfLink.templateRequired[0]];
    //   actions.push(new Action('Select', '', () => console.log(selfIdentifier)));
    // }
    if (this.constructor.schema.links === undefined) {
      return actions;
    }
    for (let link of this.constructor.schema.links) {
      const url = this.getAbsoluteUrl(link);
      switch (link.rel) {
        case 'self':
          if (this.dataset['id'] !== undefined && this.constructor.mode !== 'cell') {
            actions.push(new RefreshAction(this, url));
            if (!this.constructor.schema.readonly) {
              actions.push(new SaveAction(this, url));
              actions.push(new DeleteAction(this, url));
            }
          }
          break;
        case 'collection':
          // TODO: Browse collection
          if (this.dataset['id'] === undefined) {
            actions.push(new CreateAction(this, url + '/')); //TODO: getAbsoluteUrl doesn't seem to work with collection
          }
          if (this.constructor.mode === 'cell') {
            actions.push(new BrowseAction(new URL(link.href, this.constructor.schema.base)));
          }
          return actions;
        default:
          break;
      }
    }
    return actions;
  }

  /**
   * Resolves a JSON Schema link, that may or may not be relative, into an absolute URL with variables substituted in
   *
   * @param {object} link
   * @returns {URL}
   */
  getAbsoluteUrl(link) {
    const baseUrl = this.constructor.schema.base || this.url.replace(/\/?$/, '/'); // it needs a trailing slash to be a base. TODO: trying to get away from this magic base URL property
    const uriVariables = link.templateRequired || [];
    const url = uriVariables.reduce( (url, key) => url.replace(`{${key}}`, this.dataset[key]), link.href );
    return new URL(url, baseUrl);
  }

  renderOn(view) {
    view.renderEntity(this);
  }

  clone() {
    return new this.constructor(this.dataset);
  }

}

customElements.define(ItemComponent.elementName, ItemComponent);

import BaseComponent from '../abstract/BaseComponent.mjs';
import Clonable from '../../mixins/Clonable.mjs';
import ItemComponent from './ItemComponent.mjs';

export default class ItemCollectionComponent extends Clonable(BaseComponent) {

  static get elementName() { return 'uw-items'; }

  // TODO: this is not a great name, consider making the whole class a mixin
  static get elementComponent() { return ItemComponent; }

  static get template() {
    if (this._template === undefined) {
      this._template = document.createElement('template');
      this._template.innerHTML = `
        <style>
          .items-grid {
            display: grid;
            grid-template-columns: 20px;
          }
        </style>
        `
    }
    return this._template;
  }

  constructor(objects) {
    super(objects);
    this.elements = objects;
    const {origin, pathname, search} = new URL(objects.url);
    this.search = search;
    const itemsGrid = document.createElement('div');
    itemsGrid.className = 'items-grid';
    const baseUrl = `${origin}/${pathname}/`.replace(/\/+/g, '/').replace('http:/', 'http://').replace('https:/','https://');
    const collectionTitle = document.createElement('h2');
    collectionTitle.appendChild(document.createTextNode(this.constructor.schema.title));
    this.shadowRoot.appendChild(collectionTitle);
    itemsGrid.appendChild(new this.constructor.headerComponent({}));
    objects
      .map(object => { object.url = new URL(object.id, baseUrl).toString(); return object; } )
      .map(object => new this.constructor.elementComponent(object))
      .forEach(element => itemsGrid.appendChild(element))
    ;
    const emptyItemComponent = new this.constructor.elementComponent({});
    itemsGrid.appendChild(emptyItemComponent);
    this.shadowRoot.appendChild(itemsGrid);
  }

  // TODO
  async retrieveAll() {
    //fetch(`${this.url}?${this.search}`)
  }

  renderOn(view) {
    view.renderCollection(this);
  }

  clone() {
    return new this.constructor(this.elements);
  }

}

customElements.define(ItemCollectionComponent.elementName, ItemCollectionComponent);

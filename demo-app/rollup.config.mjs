import json from 'rollup-plugin-json';
import { terser } from 'rollup-plugin-terser';

export default {

  input: 'src/main.mjs',
  output: {
    file: 'dist/bundle.mjs',
    format: 'es'
  },

  plugins: [
    json({
      preferConst: true,
      indent: '  ',
      compact: true
    }),
    terser()
  ]

};
